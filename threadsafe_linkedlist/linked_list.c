#include <stdio.h>
#include <stdlib.h>
#include "linked_list.h"

// curr, next, and tmp are assumed to be pointers; they are not labeled;


void list_init(list **ptr_ptr_list){
    *ptr_ptr_list = malloc(sizeof(list));
    list *ptr_list = *ptr_ptr_list;
    ptr_list->head = malloc(sizeof(list_node));
    ptr_list->tail = malloc(sizeof(list_node));
    ptr_list->head->next = ptr_list->tail;
    ptr_list->num_elts = 0;
}

void list_insert(list *ptr_list, int key, int value){
    // note: this iteration method requires two pointer dereferences
    // to do a key comparison;
    // a better implementation might simply keep the previous node's
    // address stored in a variable;
    list_node *curr = ptr_list->head;
    while (curr->next != ptr_list->tail){
        if(curr->next->key == key){
            curr->next->value = value;
            printf("overwrote a value for key %d\n", key);
            return;
        }
        else if(curr->next->key > key){
            list_node *tmp = malloc(sizeof(list_node));
            tmp->key = key;
            tmp->value = value;
            tmp->next = curr->next;
            curr->next = tmp;
            ptr_list->num_elts++;
            return;
        }
        curr = curr->next;
    }

    // reached end of list, did not find new key's successor;
    // add new key to end of list;
    list_node *tmp = malloc(sizeof(list_node));
    tmp->key = key;
    tmp->value = value;
    tmp->next = curr->next;
    curr->next = tmp;
    ptr_list->num_elts++;
    return;
}

void list_delete(list *ptr_list, int key){
    list_node *curr = ptr_list->head;
    while(curr->next != ptr_list->tail){
        if(curr->next->key == key){
            list_node *tmp = curr->next;
            curr->next = curr->next->next;
            free(tmp);
            ptr_list->num_elts--;
            return;
        }
        else{
            curr = curr->next;
        }
    }
    printf("key %d not found to delete\n", key);
}

void list_print_keys_values(list *ptr_list){
    list_node *curr = ptr_list->head;
    while(curr->next != ptr_list->tail){
        printf("(%d,%d) ", curr->next->key, curr->next->value);
        curr = curr->next;
    }
    printf("end of list\n");
}


