#ifndef TS_LINKED_LIST_H
#define TS_LINKED_LIST_H

#include <pthread.h>

typedef struct ts_list_node_struct{
    pthread_mutex_t lock;
	int key;
    int value;
	struct ts_list_node_struct *next;
} ts_list_node;

typedef struct ts_list_struct{
    ts_list_node *head;
    ts_list_node *tail;
    int num_elts;
} ts_list;


void ts_list_init(ts_list **ptr_ptr_tslist);
void ts_list_insert(ts_list *ptr_tslist, int key, int value);
void ts_list_delete(ts_list *ptr_tslist, int key);
void ts_list_print_keys_values(ts_list *ptr_tslist);

void ts_check_length(ts_list *ptr_tslist, int *ret);
void ts_check_ordered(ts_list *ptr_tslist, int *ret);



#endif
