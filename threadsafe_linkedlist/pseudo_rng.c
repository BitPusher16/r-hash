#include <stdlib.h>
#include "pseudo_rng.h"

void prng_init(prng **ptr_ptr_prng, int arg_seed){
    prng *tmp = malloc(sizeof(prng));
    tmp->seed = arg_seed;
    *ptr_ptr_prng = tmp;
}

void prng_get(prng *ptr_prng, unsigned int *ret){
    // x_n+1 = (a*x_n + c) % m;
    // https://en.wikipedia.org/wiki/Linear_congruential_generator (Num Rec)
    // no need for mod because m=2^32, and unsigned int is 32 bits;
    ptr_prng->seed = (1664525 * ptr_prng->seed + 1013904223);
    *ret = ptr_prng->seed;
}
