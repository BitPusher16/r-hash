#ifndef PSEUDO_RNG_H
#define PSEUDO_RNG_H

typedef struct prng_struct{
    int seed;
} prng;

void prng_init(prng **ptr_ptr_prng, int seed);
void prng_get(prng *ptr_prng, unsigned int *ret);

#endif
