#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "ts_linked_list.h"

// curr, next, tmp, and ret are assumed to be pointers; they are not labeled;


void ts_list_init(ts_list **ptr_ptr_tslist){
    *ptr_ptr_tslist = malloc(sizeof(ts_list));
    ts_list *ptr_tslist = *ptr_ptr_tslist;
    ptr_tslist->head = malloc(sizeof(ts_list_node));
    ptr_tslist->tail = malloc(sizeof(ts_list_node));
    ptr_tslist->head->next = ptr_tslist->tail;
    ptr_tslist->num_elts = 0;
    pthread_mutex_init(&(ptr_tslist->head->lock), NULL);
    pthread_mutex_init(&(ptr_tslist->tail->lock), NULL);
}

void ts_list_insert_attempt(ts_list *ptr_tslist, int key, int value,
    int *success);

void ts_list_insert(ts_list *ptr_tslist, int key, int value){
    // attempt a threadsafe insertion a limited number of times;
    // if no attempts succeed, print an error message and return;
    int attempts = 1000000;
    int success = 0;
    for(int i = 0; i < attempts; i++){
        ts_list_insert_attempt(ptr_tslist, key, value, &success);
        if(success == 1){
            return;
        }
    }
    printf("was not able to insert (%d,%d) after %d attempts\n",
        key, value, attempts);
    exit(-1);
}

void ts_list_insert_attempt(ts_list *ptr_tslist, int key, int value,
    int *success){
    // note: this iteration method requires two pointer dereferences
    // to do a key comparison;
    // a better implementation might simply keep the previous node's
    // address stored in a variable;
    ts_list_node *curr = ptr_tslist->head;
    while (curr->next != ptr_tslist->tail){

        if(curr->next->key == key){
            // attempt to acquire a lock on the predecessor of the node
            // about to be modified;
            int acquired = pthread_mutex_trylock(&(curr->lock));
            if(acquired == 0){ 
                // lock acquired;
                curr->next->value = value;
                printf("overwrote a value for key %d\n", key);
                pthread_mutex_unlock(&(curr->lock));
                *success = 1;
            }
            else{
                *success = 0;
            }
            return;
        }

        else if(curr->next->key > key){
            // attempt to acquire a lock on the predecessor of the node
            // about to be modified;
            int acquired = pthread_mutex_trylock(&(curr->lock));
            if(acquired == 0){
                // lock acquired;
                ts_list_node *tmp = malloc(sizeof(ts_list_node));
                pthread_mutex_init(&(tmp->lock), NULL);
                tmp->key = key;
                tmp->value = value;
                tmp->next = curr->next;
                curr->next = tmp;
                ptr_tslist->num_elts++;
                pthread_mutex_unlock(&(curr->lock));
                *success = 1;
            }
            else{
                *success = 0;
            }
            return;
        }
        curr = curr->next;
    }

    // reached end of list, did not find new key's successor;
    // add new key to end of list;
    int acquired = pthread_mutex_trylock(&(curr->lock));
    if(acquired == 0){
        // lock acquired;
        ts_list_node *tmp = malloc(sizeof(ts_list_node));
        pthread_mutex_init(&(tmp->lock), NULL);
        tmp->key = key;
        tmp->value = value;
        tmp->next = curr->next;
        curr->next = tmp;
        ptr_tslist->num_elts++;
        pthread_mutex_unlock(&(curr->lock));
        *success = 1;
    }
    else{
        *success = 0;
    }
    
    return;
}

// TODO: this is not threadsafe yet;
void ts_list_delete(ts_list *ptr_tslist, int key){
    ts_list_node *curr = ptr_tslist->head;
    while(curr->next != ptr_tslist->tail){
        if(curr->next->key == key){
            ts_list_node *tmp = curr->next;
            curr->next = curr->next->next;
            free(tmp);
            ptr_tslist->num_elts--;
            return;
        }
        else{
            curr = curr->next;
        }
    }
    printf("key %d not found to delete\n", key);
}

// TODO: this is not threadsafe yet;
void ts_list_print_keys_values(ts_list *ptr_tslist){
    ts_list_node *curr = ptr_tslist->head;
    while(curr->next != ptr_tslist->tail){
        printf("(%d,%d) ", curr->next->key, curr->next->value);
        curr = curr->next;
    }
    printf("end of list\n");
}

// NOTE: this function is not meant to be threadsafe;
void ts_check_length(ts_list *ptr_tslist, int *ret){
    ts_list_node *curr = ptr_tslist->head; // okay to start at head;
    int count = 0;
    while(curr->next != ptr_tslist->tail){
        count++;
        curr = curr->next;
    }
    *ret = count;
}

// NOTE: This function is not meant to be threadsafe;
void ts_check_ordered(ts_list *ptr_tslist, int *ret){
    ts_list_node *curr = ptr_tslist->head->next; // note: starting at next;
    while(curr->next != ptr_tslist->tail){
        if(curr->key > curr->next->key){
            *ret = 0;
            return;
        }
        curr = curr->next;
    }
    *ret = 1;
}


