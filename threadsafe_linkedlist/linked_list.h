#ifndef LINKED_LIST_H
#define LINKED_LIST_H

typedef struct list_node_struct{
	int key;
    int value;
	struct list_node_struct *next;
} list_node;

typedef struct list_struct{
    list_node *head;
    list_node *tail;
    int num_elts;
} list;


void list_init(list **ptr_ptr_list);
void list_insert(list *ptr_list, int key, int value);
void list_delete(list *ptr_list, int key);
void list_print_keys_values(list *ptr_list);



#endif
