#include <stdio.h>
#include <stdatomic.h>

int main(int argc, char **argv){
    printf("hello\n");

    //_Atomic int x = 3;
    int x = 3;
    int y = 3;
    //compare_and_swap(&x, 3, 8);
    atomic_compare_exchange_strong(&x, &y, 8);
    printf("%d\n", x);
    return 0;
}
