#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <limits.h>
//#include <time.h>
#include <sys/time.h>
#include "linked_list.h"
#include "ts_linked_list.h"
#include "pseudo_rng.h"

typedef struct pthread_args_struct{
    long tid;
    int *full;
    int *starting_points;
    int *stopping_points;
    ts_list *ptr_tslist;
} pthread_args;

void *insert_nums(void *ptr_args_struct){
    //long tid = (long)threadid; // use long so no casting to diff size warning;
    pthread_args *ptr_args = (pthread_args *)ptr_args_struct;
    int t = (int)(ptr_args->tid);
    int *full = ptr_args->full;
    int *starting = ptr_args->starting_points;
    int *stopping = ptr_args->stopping_points;
    ts_list *threadtslist = ptr_args->ptr_tslist;

    //printf("hi, i'm thread %d, i'm responsible for ", t);
    printf("thread %d starting\n", t);
    for(int i = starting[t]; i <= stopping[t]; i++){
        //printf("%d ", full[i]);
        ts_list_insert(threadtslist, full[i], full[i]);
    }
    printf("thread %d exiting\n", t);
    pthread_exit(NULL);
}

int main(int argc, char **argv){
    printf("hello\n");
    list *mylist;
    list_init(&mylist);
    list_insert(mylist, 3, 103);
    list_insert(mylist, 7, 107);
    list_insert(mylist, 4, 104);
    list_insert(mylist, 2, 102);
    list_insert(mylist, 4, 1044);
    list_insert(mylist, 9, 109);
    list_insert(mylist, 11, 111);
    list_insert(mylist, 13, 113);
    printf("num list elts: %d\n", mylist->num_elts);
    list_print_keys_values(mylist);
    list_delete(mylist, 9);
    list_delete(mylist, 2);
    list_delete(mylist, 13);
    list_delete(mylist, 87);
    printf("num list elts: %d\n", mylist->num_elts);
    list_print_keys_values(mylist);

    // generate k keys (and values);
    // divide the keys among p threads;
    // ask each thread to insert its values into a thread-safe linked list;

    prng *rng;
    unsigned int r;
    //prng_init(&rng, 1337);
    prng_init(&rng, 88245);
    prng_get(rng, &r);
    printf("random number: %u\n", r);
    prng_get(rng, &r);
    printf("random number: %u\n", r);
    prng_get(rng, &r);
    printf("random number: %u\n", r);
    prng_get(rng, &r);
    printf("random number: %u\n", r);

    //int k = 100;
    int k = 100000;
    int full_arr[k];
    for(int i = 0; i < k; i++){
        prng_get(rng, &r);
        full_arr[i] = r % INT_MAX;
        //full_arr[i] = i;
    }

    int p = 12;
    int keys_per_thread = k / p;
    int thread_starting_points[k];
    int thread_stopping_points[k];
    for(int i = 0; i < p; i++){
        thread_starting_points[i] = i * keys_per_thread;
        thread_stopping_points[i] = thread_starting_points[i] + 
            keys_per_thread - 1;
    }
    thread_stopping_points[p-1] = k-1;

    ts_list *mytslist;
    ts_list_init(&mytslist);
    /*
    ts_list_insert(mytslist, 3, 103);
    ts_list_insert(mytslist, 7, 107);
    ts_list_insert(mytslist, 9, 109);
    printf("num ts list elts: %d\n", mytslist->num_elts);
    ts_list_print_keys_values(mytslist);
    ts_list_delete(mytslist, 7);
    printf("num ts list elts: %d\n", mytslist->num_elts);
    ts_list_print_keys_values(mytslist);
    */


    //clock_t begin = clock();
    struct timeval tv1, tv2;
    gettimeofday(&tv1, NULL);

    pthread_t threads[p];
    long t;
    int rc;
    for(t = 0; t < p; t++){

        //printf("creating thread %ld\n", t);
        pthread_args *ptr_args = malloc(sizeof(pthread_args));
        ptr_args->tid = t;
        ptr_args->full = full_arr;
        ptr_args->starting_points = thread_starting_points;
        ptr_args->stopping_points = thread_stopping_points;
        ptr_args->ptr_tslist = mytslist;

        rc = pthread_create(&threads[t], NULL, insert_nums, (void *)ptr_args);
        if(rc){
            printf("ERROR; return code from pthread_create is %d\n", rc);
            exit(-1);
        }
    }
    // wait for threads to finish;
    for(int i = 0; i < p; i++){
        pthread_join(threads[i], NULL);
    }

    //clock_t end = clock();
    //double time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
    //printf("secs: %f\n", time_spent);
    gettimeofday(&tv2, NULL);
    printf("seconds %f\n", (double)(tv2.tv_usec - tv1.tv_usec) / 1000000 +
        (double)(tv2.tv_sec - tv1.tv_sec));
    
    // check list status;
    printf("num ts list elts: %d\n", mytslist->num_elts);
    //ts_list_print_keys_values(mytslist);
    int length = 0;
    ts_check_length(mytslist, &length);
    printf("checked length: %d\n", length);
    int ordered = 0;
    ts_check_ordered(mytslist, &ordered);
    if(ordered == 1){
        printf("final list has correct order\n");
    }
    else{
        printf("final list has incorrect order\n");
    }

    pthread_exit(NULL);

}
