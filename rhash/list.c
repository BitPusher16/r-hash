#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "list.h"

void list_init(list **ptr_ptr_list){
    *ptr_ptr_list = malloc(sizeof(list));
    list *pl = *ptr_ptr_list;
    pl->curr_elts = 0;
    pl->handle = malloc(sizeof(list_elt));
    pl->tail = pl->handle;
}

void list_final(list **ptr_ptr_list){
    list *pl = *ptr_ptr_list;
    list_elt *curr;
    list_elt *next;
    curr = pl->handle;
    while(curr != NULL){
        next = curr->next;
        free(curr);
        curr = next;
    }
    free(pl);
    *ptr_ptr_list = 0;
}

void list_append(list *ptr_list, unsigned int key, unsigned int *val){
    list_elt *tmp = malloc(sizeof(list_elt));
    tmp->key = key;
    tmp->val = val;
    ptr_list->tail->next = tmp;
    ptr_list->tail = tmp;
    ptr_list->curr_elts++;
}

void list_print(list *ptr_list){
    list *pl = ptr_list;
    printf("%d elts: ", pl->curr_elts);
    list_elt *curr = pl->handle->next;
    while(curr != NULL){
        printf("(%u,%ld) ", curr->key, (intptr_t)(curr->val));
        curr = curr->next;
    }
    printf("\n");
}
