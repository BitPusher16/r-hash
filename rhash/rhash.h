#ifndef RHASH_H
#define RHASH_H

#include <limits.h>
#include "cuckoo.h"
#include "list.h"
#include "prng.h"

// the cuckoo hash cannot store 0,
// and we will use the min and max possible key values to initialize
// r-hash, so r-hash cannot store keys 0, 1, or 2^32-1;
#define RMIN 1
#define RMAX UINT_MAX

typedef struct rhash_struct{   
    cuckoo *base;
    struct rhash_struct *guide;

    prng *rndg;
    // 1/128; numerator should always be 1, is not needed;
    int promote_prob_den;
} rhash;

void rhash_init(rhash **ptr_ptr_rhash);
void rhash_final(rhash **ptr_ptr_rhash);
void rhash_insert(rhash *ptr_rhash, unsigned int key, unsigned int *val);
void rhash_delete(rhash *ptr_rhash, unsigned int key);
void rhash_search_pt(rhash *ptr_rhash, unsigned int key, unsigned int **ret);
void rhash_search_rng(rhash *ptr_rhash, unsigned int key_min,
    unsigned int key_max, list **ptr_ptr_list);

// to get the number of elements stored in rhash pr, use:
// pr->base->curr_elts - 2;

#endif
