#ifndef PRNG_H
#define PRNG_H

#include <limits.h>

#define PRNG_MAX UINT_MAX

typedef struct prng_struct{
    int seed;
} prng;

void prng_init(prng **ptr_ptr_prng, int seed);
void prng_final(prng **ptr_ptr_prng);
void prng_get(prng *ptr_prng, unsigned int *ret);

#endif
