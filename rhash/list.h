#ifndef LIST_H
#define LIST_H

// singly-linked list;

typedef struct list_elt_struct{
    unsigned int key;
    unsigned int *val;
    struct list_elt_struct *next;
} list_elt;

typedef struct list_struct{
    int curr_elts;
    list_elt *handle; // (distinct from head)
    list_elt *tail;
} list;

void list_init(list **ptr_ptr_list);
void list_final(list **ptr_ptr_list);
void list_append(list *ptr_list, unsigned int key, unsigned int *val);
void list_print(list *ptr_list);

#endif
