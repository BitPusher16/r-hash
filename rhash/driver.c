#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "cuckoo.h"
#include "prng.h"
#include "list.h"
#include "rhash.h"

typedef struct test_elt_struct{
    // 0 = insert, 1 = delete;
    int instruction;
    unsigned int key;
    unsigned int *val;
} test_elt;

int main(int argc, char **argv){

    // generate an array of key-value pairs for testing;
    int num_inserts = 2400;
    prng *rndg;
    prng_init(&rndg, 4321);
    unsigned int rnd;
    test_elt *insert_arry = malloc(num_inserts * sizeof(test_elt));
    for(int i = 0; i < num_inserts; i++){
        insert_arry[i].instruction = 0;
        prng_get(rndg, &rnd);
        insert_arry[i].key = rnd;
        insert_arry[i].val = (unsigned int *)((intptr_t)rnd);
    }

    // test cuckoo hash
    cuckoo *c;
    cuckoo_init(&c);

    cuckoo_insert(c, 3, (unsigned int *)103);
    cuckoo_insert(c, 5, (unsigned int *)105);

    unsigned int *val;
    cuckoo_search(c, 3, &val);
    printf("cuckoo_search() returned %p, d%ld\n", val, (intptr_t)val);

    cuckoo_delete(c, 3);
    cuckoo_search(c, 3, &val);
    printf("cuckoo_search() returned %p, d%ld\n", val, (intptr_t)val);


    for(int i = 0; i < num_inserts; i++){
        cuckoo_insert(c, insert_arry[i].key, insert_arry[i].val);
    }

    cuckoo_search(c, insert_arry[0].key, &val);
    printf("cuckoo_search() on key %u returned %p, d%ld\n", 
        insert_arry[0].key, val, (intptr_t)val);
    printf("elts in cuckoo hash: %d\n", c->curr_elts);

    cuckoo_final(&c);
    prng_final(&rndg);

    // test list;
    list *pl;
    list_init(&pl);
    list_append(pl, 8, (unsigned int*)108);
    list_append(pl, 3, (unsigned int*)103);
    list_append(pl, 7, (unsigned int*)107);

    list_print(pl);
    list_final(&pl);

    // test rhash;
    rhash *pr;
    rhash_init(&pr);

    rhash_insert(pr, 8, (unsigned int*)108);
    rhash_insert(pr, 2, (unsigned int*)102);
    rhash_insert(pr, 7, (unsigned int*)107);
    rhash_insert(pr, 11, (unsigned int*)111);
    rhash_insert(pr, 21, (unsigned int*)121);
    rhash_insert(pr, 14, (unsigned int*)114);
    rhash_insert(pr, 16, (unsigned int*)116);
    rhash_insert(pr, 24, (unsigned int*)124);
    rhash_insert(pr, 28, (unsigned int*)128);

    for(int i = 0; i < num_inserts; i++){
        rhash_insert(pr, insert_arry[i].key, insert_arry[i].val);
    }

    rhash_search_pt(pr, 16, &val);
    printf("rhash_search() on key %u returned %p, d%ld\n", 
        16, val, (intptr_t)val);

    rhash_search_pt(pr, insert_arry[0].key, &val);
    printf("rhash_search() on key %u returned %p, d%ld\n", 
        insert_arry[0].key, val, (intptr_t)val);

    // test range search on rhash;
    // TODO
    list *plist1;


    rhash_final(&pr);
    //list_final(&plist1);
    
}
