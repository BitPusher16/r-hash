#include <stdlib.h>
#include "rhash.h"
#include "cuckoo.h"
#include "prng.h"

/**********
* internal functions;
**********/

// searches an rhash to find the largest key smaller than the argument key;
// begins searching at the start key;
void get_prev_with_start(rhash *ptr_rhash, unsigned int key,
    unsigned int *prev, unsigned int start){
    rhash *pr = ptr_rhash;

    unsigned int curr_key = start;
    unsigned int next_key;
    cuckoo_get_next(pr->base, curr_key, &next_key);
    while(next_key > key){
        curr_key = next_key;
        cuckoo_get_next(pr->base, curr_key, &next_key);
    }
    *prev = curr_key;
}

void get_prev(rhash *ptr_rhash, unsigned int key, unsigned int *prev){
    rhash *pr = ptr_rhash;
    unsigned int starting_key = RMIN;
    // if the rhash has a guide rhash, consult it;
    if(pr->guide != NULL){
        get_prev(pr->guide, key, &starting_key);
    }
    // get the previous key, using the starting key;
    get_prev_with_start(pr, key, prev, starting_key);
    return;
}

void r_init(rhash **ptr_ptr_rhash, unsigned int seed){
    *ptr_ptr_rhash = malloc(sizeof(rhash));
    rhash *pr = *ptr_ptr_rhash;
    cuckoo_init(&(pr->base));
    // insert min, max keys and link;
    cuckoo_insert(pr->base, RMIN, 0);
    cuckoo_insert(pr->base, RMAX, 0);
    cuckoo_set_next(pr->base, RMIN, RMAX);

    pr->guide = NULL;

    prng_init(&pr->rndg, seed);
    pr->promote_prob_den = 2048;
}

/**********
* external functions;
**********/

void rhash_init(rhash **ptr_ptr_rhash){
    r_init(ptr_ptr_rhash, 2345);
}

void rhash_final(rhash **ptr_ptr_rhash){
    rhash *pr = *ptr_ptr_rhash;
    cuckoo_final(&(pr->base));
    if(pr->guide != NULL){ rhash_final(&(pr->guide)); }
    prng_final(&pr->rndg);
    *ptr_ptr_rhash = NULL;
}

void rhash_insert(rhash *ptr_rhash, unsigned int key, unsigned int *val){
    rhash *pr = ptr_rhash;
    // insert new key-value pair;
    cuckoo_insert(pr->base, key, val);
    // perform linking;
    unsigned int prev_key, next_key;
    get_prev(pr, key, &prev_key);
    cuckoo_get_next(pr->base, prev_key, &next_key);
    cuckoo_set_next(pr->base, prev_key, key);
    cuckoo_set_next(pr->base, key, next_key);

    // with some probability, promote the key to the next level;
    unsigned int rnd;
    prng_get(pr->rndg, &rnd);
    if(rnd < PRNG_MAX / pr->promote_prob_den){
        if(pr->guide == NULL){
            // generate a new seed for the guide rhash;
            unsigned int new_seed;
            prng_get(pr->rndg, &new_seed);

            // initialize the guide rhash
            r_init(&(pr->guide), new_seed);
        }
        rhash_insert(pr->guide, key, val);
    }
}

void rhash_delete(rhash *ptr_rhash, unsigned int key){
    rhash *pr = ptr_rhash;

    if(pr->guide != NULL){
        rhash_delete(pr->guide, key);
    }

    unsigned int prev_key, next_key;
    get_prev(pr, key, &prev_key);
    cuckoo_get_next(pr->base, prev_key, &next_key);
    cuckoo_set_next(pr->base, prev_key, next_key);
    cuckoo_delete(pr->base, key);
}

void rhash_search_pt(rhash *ptr_rhash, unsigned int key, unsigned int **ret){
    cuckoo_search(ptr_rhash->base, key, ret);
}

void rhash_search_rng(rhash *ptr_rhash, unsigned int key_min,
    unsigned int key_max, list **ptr_ptr_list){

    rhash *pr = ptr_rhash;
    list *pl;
    list_init(&pl);

    unsigned int prev_key, curr_key;
    unsigned int *curr_val;
    get_prev(pr, key_min, &prev_key);
    cuckoo_get_next(pr->base, prev_key, &curr_key);
    while(curr_key <= key_max){
        // append key-val pair to return list;
        cuckoo_search(pr->base, curr_key, &curr_val);
        list_append(pl, curr_key, curr_val);
        // move curr_key, next_key forward;
        cuckoo_get_next(pr->base, curr_key, &curr_key);
    }
    *ptr_ptr_list = pl;
    return;
}
