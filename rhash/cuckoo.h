#ifndef CUCKOO_H
#define CUCKOO_H

// See the paper Cuckoo Hashing, by Pagh and Rodler.
// As of 2017-01-28, Wikipedia explanation of cuckoo hashing is misleading.
// We choose cuckoo null to be 0 because that is the default value stored
// in each memory word by calloc().
// This means our cuckoo hash cannot store a key-value pair with key 0.

#include "prng.h"
#define CNULL 0

typedef struct cuckoo_elt_struct{
    unsigned int key;
    unsigned int *val;
    // this member is needed by rhash;
    unsigned int next;
} cuckoo_elt;

typedef struct cuckoo_struct{
    // 2^table_pow is the size of one table;
    int min_table_pow;
    int table_pow;
    // number of occupied elts (sum from both tables);
    int curr_elts;
    // max number of attempted insertion loops is limited to some small 
    // constant times the log (base some small epsilon) of the number of elts
    // currently occupied in both tables;
    // see https://web.stanford.edu/class/cs166/lectures/13/Small13.pdf;
    // for simplicity, set this instead to some small constant
    // times log_2 the sum of the sizes of both tables;
    int max_loop_const;
    int max_loop;

    // 7/16 (must be < 1/2)
    int max_load_num;
    int max_load_den;

    // 1/16
    int min_load_num;
    int min_load_den;

    // pseudo random number generator is used to generate hash constants;
    // in turn, hash constants are used to hash integers to buckets;
    // see https://en.wikipedia.org/wiki/Universal_hashing#Hashing_integers
    // choose hash constants that are odd and fall between 2^32*5/8
    // and 2^32*3/8;
    prng *pseudo_gen;
    unsigned int hash_const_A;
    unsigned int hash_const_B;
    cuckoo_elt *table_A;
    cuckoo_elt *table_B;

} cuckoo;

void cuckoo_init(cuckoo **ptr_ptr_cuckoo);
void cuckoo_final(cuckoo **ptr_ptr_cuckoo);
void cuckoo_insert(cuckoo *ptr_cuckoo, unsigned int key, unsigned int *val);
void cuckoo_search(cuckoo *ptr_cuckoo, unsigned int key, unsigned int **ret);
void cuckoo_delete(cuckoo *ptr_cuckoo, unsigned int key);

// these functions are needed by rhash;
void cuckoo_get_next(cuckoo *ptr_cuckoo, unsigned int key, unsigned int *ret);
void cuckoo_set_next(cuckoo *ptr_cuckoo, unsigned int key, unsigned int next);


#endif
