#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include "cuckoo.h"
#include "prng.h"

typedef struct test_elt_struct{
    // 0 = insert, 1 = delete;
    int instruction;
    unsigned int key;
    unsigned int *val;
} test_elt;

int main(int argc, char **argv){
    printf("hello\n");

    cuckoo *c;
    cuckoo_init(&c);

    cuckoo_insert(c, 3, (unsigned int *)103);
    cuckoo_insert(c, 5, (unsigned int *)105);

    unsigned int *val;
    cuckoo_search(c, 3, &val);
    printf("cuckoo_search() returned %p, d%ld\n", val, (intptr_t)val);

    cuckoo_delete(c, 3);
    cuckoo_search(c, 3, &val);
    printf("cuckoo_search() returned %p, d%ld\n", val, (intptr_t)val);

    int num_inserts = 24000;
    prng *rndg;
    prng_init(&rndg, 4321);
    unsigned int rnd;
    test_elt *insert_arry = malloc(num_inserts * sizeof(test_elt));
    for(int i = 0; i < num_inserts; i++){
        insert_arry[i].instruction = 0;
        prng_get(rndg, &rnd);
        insert_arry[i].key = rnd;
        insert_arry[i].val = (unsigned int *)((intptr_t)rnd);
    }

    for(int i = 0; i < num_inserts; i++){
        cuckoo_insert(c, insert_arry[i].key, insert_arry[i].val);
    }

    cuckoo_search(c, insert_arry[0].key, &val);
    printf("cuckoo_search() on key %u returned %p, d%ld\n", 
        insert_arry[0].key, val, (intptr_t)val);
    printf("elts in cuckoo hash: %d\n", c->curr_elts);

    cuckoo_final(&c);
}
