#include <stdio.h>
#include <stdlib.h>
#include "cuckoo.h"

/**********
* internal functions
**********/

// insert() is used in two contexts:
// 1) a call from the parent program;
// 2) an internal call during a rehash, shrink, or grow;
// in 1), if insert() fails then it should attempt
// to rehash until a successful arrangement is found;
// in 2), if insert() fails then it should stop and report the failure
// to the calling function;
// parameter on_failure tells insert() which action to take on failure;
// 0 means attempt to rehash, 1 means abort and report failure;
// parameter status is used to report success/failure to the calling function;
// 0 means success, 1 means failure;
void insert(cuckoo *ptr_cuckoo, unsigned int key, unsigned int *val,
    int on_failure, int *status);

int power(int x, int y){
    int ret = 1;
    while(y-- > 0){ ret *= x; }
    return ret;
}

void pick_hash_constants(cuckoo *ptr_cuckoo){
    cuckoo *pc = ptr_cuckoo;
    pc->hash_const_A = 0;
    pc->hash_const_B = 0;
    while(pc->hash_const_A % 2 != 1 || pc->hash_const_A > 2684354560 || 
        pc->hash_const_A < 1610612736
    ){
        prng_get(pc->pseudo_gen, &(pc->hash_const_A));
    }
    while(pc->hash_const_B % 2 != 1 || pc->hash_const_B > 2684354560 || 
        pc->hash_const_B < 1610612736
    ){
        prng_get(pc->pseudo_gen, &(pc->hash_const_B));
    }
}

// Hashes an unsigned int in range 0->2^32-1 to a
// bucket in the range 0->2^p-1.
// See https://en.wikipedia.org/wiki/Universal_hashing#Hashing_integers
unsigned int mult_hash(unsigned int a, unsigned int x, int w){
    return (a*x) >> (32 - w);
}

void init(cuckoo **ptr_ptr_cuckoo, int arg_min_table_pow, int arg_seed){
    *ptr_ptr_cuckoo = malloc(sizeof(cuckoo));
    cuckoo *pc = *ptr_ptr_cuckoo;

    pc->min_table_pow = arg_min_table_pow;
    pc->table_pow = pc->min_table_pow;
    pc->curr_elts = 0;
    pc->max_loop_const = 32;
    pc->max_loop = pc->max_loop_const * pc->table_pow;
    pc->max_load_num = 7;
    pc->max_load_den = 16;
    pc->min_load_num = 1;
    pc->min_load_den = 16;

    // pseudo-random number generator is used to generate hash constants;
    pc->pseudo_gen = malloc(sizeof(prng));
    prng_init(&(pc->pseudo_gen), arg_seed);

    // populate hash constants;
    pick_hash_constants(pc);
    //printf("hash_const_A set to %u\n", pc->hash_const_A);
    //printf("hash_const_B set to %u\n", pc->hash_const_B);

    pc->table_A = calloc(power(2,pc->table_pow), sizeof(cuckoo_elt));
    pc->table_B = calloc(power(2,pc->table_pow), sizeof(cuckoo_elt));
}

// migrates key-value pairs from one cuckoo hash to another;
// migrate() is used during rehash, grow, or shrink;
// setting status = 0 means success, setting status = 1 means failure;
void migrate(cuckoo *from, cuckoo *to, int *status){
    int on_insert_failure = 1; // stop, report failure;
    int insert_status = 0;
    int i;
    // attempt to migrate from->table_A
    for(i = 0; i < power(2,from->table_pow); i++){
        if( (from->table_A)[i].key != CNULL){
            insert(to, (from->table_A)[i].key, (from->table_A)[i].val,
                on_insert_failure, &insert_status);
            if(insert_status == 1){ break; }
        }
    }
    if(insert_status == 1){ *status = 1; return; }
    // attempt to migrate from->table_B
    for(i = 0; i < power(2,from->table_pow); i++){
        if( (from->table_B)[i].key != CNULL){
            insert(to, (from->table_B)[i].key, (from->table_B)[i].val,
                on_insert_failure, &insert_status);
            if(insert_status == 1){ break; }
        }
    }
    if(insert_status == 1){ *status = 1; return; }

    *status = 0;
    return;
}

// there is ambiguity in the Pagh and Rodler paper regarding the rehash func;
// when they re-hash, do they fix collisions by bouncing back and forth,
// as in insert(), or do they simply try table A and table B and then give up?
// this code implements the former approach by declaring a 
// completely new cuckoo hash (same table sizes) and attempting
// to migrate all entries from old to new;
// note, this is the same functionality needed by grow and shrink,
// so we add a size argument the set the size of the new hash;
void rehash(cuckoo *ptr_cuckoo, int arg_table_pow){
    cuckoo *pc = ptr_cuckoo;
    cuckoo *tmp;
    int status = 1;
    while(status == 1){
        printf("attempting rehash pow %d to %d\n", 
            pc->table_pow, arg_table_pow);
        unsigned int rand;
        prng_get(pc->pseudo_gen, &rand);
        init(&tmp, arg_table_pow, rand);
        migrate(pc, tmp, &status);
        if(status == 1){ cuckoo_final(&tmp); }
    }

    free(pc->pseudo_gen);
    free(pc->table_A);
    free(pc->table_B);

    pc->pseudo_gen = tmp->pseudo_gen;
    pc->table_A = tmp->table_A;
    pc->table_B = tmp->table_B;
    pc->hash_const_A = tmp->hash_const_A;
    pc->hash_const_B = tmp->hash_const_B;

    // update table size, max loop;
    pc->table_pow = arg_table_pow;
    pc->max_loop = pc->max_loop_const * pc->table_pow;

    free(tmp);
    return;
}

void resize(cuckoo *ptr_cuckoo){
    cuckoo *pc = ptr_cuckoo;
    if(pc->curr_elts > power(2,pc->table_pow) * 
        pc->max_load_num / pc->max_load_den){
        rehash(pc, pc->table_pow + 1);
    }
    if(pc->curr_elts < power(2,pc->table_pow) * 
        pc->min_load_num / pc->min_load_den 
        && pc->table_pow > pc->min_table_pow
    ){
        rehash(pc, pc->table_pow - 1);
    }
}

void insert(cuckoo *ptr_cuckoo, unsigned int key, unsigned int *val,
    int on_failure, int *status){
    // the core code for this function is taken directly from
    // Pagh and Rodler's paper;

    *status = 0; // success;
    cuckoo *pc = ptr_cuckoo;

    unsigned int hkA = mult_hash(pc->hash_const_A, key, pc->table_pow);
    if( (pc->table_A)[hkA].key == key){
        (pc->table_A)[hkA].val = val;
        return;
    }
    unsigned int hkB = mult_hash(pc->hash_const_B, key, pc->table_pow);
    if( (pc->table_B)[hkB].key == key){
        (pc->table_B)[hkB].val = val;
        return;
    }

    // to perform relocations, we require two cuckoo elt structs;
    // one is to hold the displaced elt, the other is to enable swaps;
    // do not malloc these objects; malloc takes time, and after
    // malloc we will have to do pointer dereferences;
    int i;
    cuckoo_elt d, s;
    d.key = key;
    d.val = val;
    for(i = 0; i < pc->max_loop; i++){
        // if slot in table A is empty, insert d and return;
        hkA = mult_hash(pc->hash_const_A, d.key, pc->table_pow);
        if( (pc->table_A)[hkA].key == CNULL){
            (pc->table_A)[hkA].key = d.key;
            (pc->table_A)[hkA].val = d.val;
            pc->curr_elts++;
            resize(pc);
            return;
        }
        // swap d and table_A[hkA];
        s.key = (pc->table_A)[hkA].key;
        s.val = (pc->table_A)[hkA].val;
        (pc->table_A)[hkA].key = d.key;
        (pc->table_A)[hkA].val = d.val;
        d.key = s.key;
        d.val = s.val;
        // if slot in table B is empty, insert d and return;
        hkB = mult_hash(pc->hash_const_B, d.key, pc->table_pow);
        if( (pc->table_B)[hkB].key == CNULL){
            (pc->table_B)[hkB].key = d.key;
            (pc->table_B)[hkB].val = d.val;
            pc->curr_elts++;
            resize(pc);
            return;
        }
        // swap d and table_B[hkB];
        s.key = (pc->table_B)[hkB].key;
        s.val = (pc->table_B)[hkB].val;
        (pc->table_B)[hkB].key = d.key;
        (pc->table_B)[hkB].val = d.val;
        d.key = s.key;
        d.val = s.val;
    }

    // we have reached end of max_iter and no empty slot was found;
    // currently, d holds the last displaced cuckoo elt;
    // if on_failure tells us to report failure and abort, do that;
    // else, re-hash the tables and then attempt to insert d (recursive call);

    if(on_failure == 1){
        *status = 1; // failure to insert new element;
        return;
    }

    rehash(pc, pc->table_pow);
    int recursive_on_failure = 0; // attempt rehash on insert failure;
    int recursive_status = 0;
    insert(pc, d.key, d.val, recursive_on_failure, &recursive_status);
    return;
}


/**********
* external functions
**********/

void cuckoo_init(cuckoo **ptr_ptr_cuckoo){
    init(ptr_ptr_cuckoo, 8, 1337);
}

void cuckoo_final(cuckoo **ptr_ptr_cuckoo){
    cuckoo *pc = *ptr_ptr_cuckoo;
    free(pc->pseudo_gen);
    free(pc->table_A);
    free(pc->table_B);
    free(pc);
    *ptr_ptr_cuckoo = 0;
}

void cuckoo_insert(cuckoo *ptr_cuckoo, unsigned int key, unsigned int *val){
    int on_failure = 0;
    int status = 0;
    insert(ptr_cuckoo, key, val, on_failure, &status);
}

void cuckoo_search(cuckoo *ptr_cuckoo, unsigned int key, unsigned int **ret){
    cuckoo *pc = ptr_cuckoo;
    unsigned int hkA = mult_hash(pc->hash_const_A, key, pc->table_pow);
    if( (pc->table_A)[hkA].key == key){
        *ret = (pc->table_A)[hkA].val;
        return;
    }
    unsigned int hkB = mult_hash(pc->hash_const_B, key, pc->table_pow);
    if( (pc->table_B)[hkB].key == key){
        *ret = (pc->table_B)[hkB].val;
        return;
    }
    *ret = (unsigned int *)0;
    return;
}

void cuckoo_delete(cuckoo *ptr_cuckoo, unsigned int key){
    cuckoo *pc = ptr_cuckoo;
    unsigned int hkA = mult_hash(pc->hash_const_A, key, pc->table_pow);
    if( (pc->table_A)[hkA].key == key){
        (pc->table_A)[hkA].key = CNULL;
        (pc->table_A)[hkA].val = CNULL;
        pc->curr_elts--;
        return;
    }
    unsigned int hkB = mult_hash(pc->hash_const_B, key, pc->table_pow);
    if( (pc->table_B)[hkB].key == key){
        (pc->table_B)[hkB].key = 0;
        (pc->table_B)[hkB].val = 0;
        pc->curr_elts--;
        return;
    }
    return;
}











